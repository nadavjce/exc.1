<h1>This is your book list</h1>
<table style="width:40%">
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
th {
    background-color: lightblue;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<tr>
    <th>Book Name</th>
    <th>Author</th> 
  </tr>
    @foreach($books as $book)
  
  <tr>
    <td>{{$book->title}}</td>
    <td>{{$book->author}}</td> 
  </tr>

    @endforeach
    </table>