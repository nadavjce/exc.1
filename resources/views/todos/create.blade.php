<h1>Create a new todo</h1>
<form method = 'post' action = "{{action('TodoController@store')}}">
{{csrf_field()}}

<div class = "firm-group">
    <label for = "title">what would you like to do?</label>
    <input type = "text" class ="form-control" name = "title">
</div>
<div class = "form-group">
    <input type = "submit" class = "form-control" name = "submit" value = "Save">
</div>
</form>