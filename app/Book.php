<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    public function user(){//יש משמעות ליוזר- הפונצקייה תדע ללכת ליוזר אי די user_id
        return $this -> belongsTo('App\User');//כל אובייקט מסוג טודו חייב להיות שייך ליוזר אחד
    }
}
