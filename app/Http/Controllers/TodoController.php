<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use App\User;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all todos
        $id =1;//simulating that user 1 is logged in
//$todos = Todo::all();
        $todos = User::find($id)->todos;//הולך לטבלת יוזר מוצא את היוזר שזה האי די שלו הולך לטבלת טודו ושולף מהטבלה את כל הטודוס שזה היוזר אי די שלו
      return view('todos.index', ['todos' => $todos]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $todo =new  Todo();
        $id = 1;
        $todo->title = $request->title;
        $todo->user_id=$id;
        $todo->save();
        return redirect('todos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        {
            $todo= Todo::find($id);
            return view ('todos.edit',compact('todo'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        {
            $todo = Todo::find($id);
            $todo->update($request->all());
            return redirect('todos');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        {
            $todo = Todo::find($id);
            $todo->delete();
            return redirect('todos');
        }
    }
}

