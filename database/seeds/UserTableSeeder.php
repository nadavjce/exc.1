<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            [
                'name'=> 'Nadav',
                'email'=>'Nadav@WT',
                'password'=>'12345',
                'created_at'=> date('Y-m-d G:i:s'),
            ],
            [
                'name'=> 'Nerya',
                'email'=>'Ner@WT0',
                'password'=>'12345',
                'created_at'=> date('Y-m-d G:i:s'),
            ],
            [
                'name'=> 'name',
                'email'=>'Name@WT2',
                'password'=>'12345',
                'created_at'=> date('Y-m-d G:i:s'), 
            ],
        ]);
    }
}
