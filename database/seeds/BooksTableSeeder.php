<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'title'=> 'Harry potter',    
                'author'=> 'JK',
                'user_id'=>1,
            ],
            [
                'title'=> 'System inventibe thinking',
                
                'author'=> 'Roni horovitz', 
                'user_id'=>1,
            ],
            [
                'title'=> 'ha matara',
                
                'author'=> 'goldrat', 
                'user_id'=>1,
            ],
            [
                'title'=> 'how to make friends and influence people',
                
                'author'=> 'carnegi', 
                'user_id'=>1,
            ],
            [
                'title'=> 'the hobbit',
                
                'author'=> 'j.r.r tolkin', 
                'user_id'=>1,
            ],
            ]);
    }
}
