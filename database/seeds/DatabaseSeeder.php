<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TodsTableSeeder::class);
        $this->call(BooksTableSeeder::class);
        $this->call(UserTableSeeder::class);

    }
}
